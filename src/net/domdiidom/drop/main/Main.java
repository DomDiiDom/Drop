package net.domdiidom.drop.main;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.domdiidom.drop.commands.CommandDrop;

public class Main extends JavaPlugin{
	
	private ConsoleCommandSender console = Bukkit.getConsoleSender();
	
	@Override
	public void onEnable() {
		console.sendMessage("�2[DROP] Enabled");
		
		this.onRegisterCommands();
	}
	
	@Override
	public void onDisable() {
		console.sendMessage("�4[DROP] Disabled");
	}
	
	private void onRegisterCommands() {
		this.getCommand("drop").setExecutor(new CommandDrop(this));
	}
	
	public static String getCardinalDirection(Player player){
		try{
			double rotation = (player.getLocation().getYaw() - 90) % 360;
			if (rotation < 0)
			    rotation += 360.0;
			if (0 <= rotation && rotation < 67.5)
			    return "NW"; //NorthWest
			else if (67.5 <= rotation && rotation < 112.5)
			    return "N"; //North 
			else if (112.5 <= rotation && rotation < 157.5)
			    return "NE"; //NorthEeast
			else if (157.5 <= rotation && rotation < 202.5)
			    return "E"; //East 
			else if (202.5 <= rotation && rotation < 247.5)
			        return "SE"; //SouthEast 
			else if (247.5 <= rotation && rotation < 292.5)
			    return "S"; //South 
			else if (292.5 <= rotation && rotation < 337.5)
			    return "SW"; //SouthWest 
			else if (337.5 <= rotation && rotation < 360.0)
			   return "W"; //West 			   
		}catch(Exception e){e.printStackTrace();}
		return null;
	}

}
