package net.domdiidom.drop.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import net.domdiidom.drop.main.Main;

public class CommandDrop implements CommandExecutor{
	Main main;
	
	public CommandDrop(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String name, String[] args) {
		if(!sender.hasPermission("drop.admin")) {
			sender.sendMessage("�4ERROR: Du hast keine berechtigung.");
			return true;
		}
		
		if(args.length == 3) {
			Player target = Bukkit.getPlayer(args[0]);
			if(target == null) {
				sender.sendMessage("�4Spieler nicht gefunden.");
				return true;
			}
			Location spawn = target.getLocation();
			Vector direction = spawn.getDirection().multiply(2);
			spawn.add(direction);
			try {
			int id = Integer.valueOf(args[1]);
			int value = Integer.valueOf(args[2]);
			
			ItemStack item = new ItemStack(id, value);		
			target.getWorld().dropItemNaturally(spawn, item);
			}catch(Exception e) {
				sender.sendMessage("�4ERROR: Bitte gib ein richtiges Item ein.");
				return true;
			}
		}
		return true;
	}

}
